#!/usr/bin/env ruby
require 'ruby_rhymes'
require 'raspell'

def rhyme_line(line)
  # Splits the line by whitespace and linebreaks
  words = line.split(/[\s\n]+/)

  # Removes words and digits, replaces them with whitespace
  result = words.map do |word|
    word.gsub(/[^\w\d]+/,'')
  end

  # Remove words that are commas
  result = result.reject do |word|
    word.strip.length.zero? || word =~ /[,'’]/
  end

  #
  result = result.map do |word|
    syllables_to_match = word.to_phrase.syllables
    rhymed = word.to_phrase.flat_rhymes.select do |rhymed_word|
      rhymed_word.to_phrase.syllables == syllables_to_match
    end
    rhymed = rhymed.select do |rhymed_word|
      suggestions = $speller.suggest(rhymed_word)
      is_spelled = rhymed_word.downcase == suggestions.first.downcase
    end.sample #End result assignment
    rhymed || word
  end
  # Put join all the words into a string
  result.join(' ')

end

$speller = Aspell.new("en_US")
$speller.suggestion_mode = Aspell::NORMAL

input = ARGF.read.strip
puts(input.lines.map do |line|
  rhyme_line(line)
end.to_a.join("\n"))
